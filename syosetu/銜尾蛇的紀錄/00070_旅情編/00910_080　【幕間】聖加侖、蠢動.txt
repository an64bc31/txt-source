大陸東方、聖加侖連邦王國。

白雪消融後嫩草開始萌發、這是生命飽滿的季節、春天。明明是晴朗的青空、但是、在那之下原野確是不是一團順利的顏色、陰暗的顏色就像是在吊喪一樣。

理由、看著他們的著裝就能夠明白。代替帽子的是一頂鋼盔、身體這鎧甲足具、手上攜帶著一把槍。這是士兵。看到那用著悲壯的表情進行行軍的身影、就不難理解這次不是單純的訓練與移動。

他們向戰場進發。

「國王和貴族、不管什麼時候都是白痴」

一位歩兵、用著陰鬱的聲音嘟噥。

「想著明明發生了魔物騒動、卻又和阿爾凱爾打戰。明明現在就應該休養生息、連邦又因為內鬥而吵了起來。就不能相互體諒一下嗎」
「喂、閉嘴」

並列的士兵、聽到後連忙制止。

「都不知道有誰會在聽。我可不想被你牽連」
「你算什麼啊」

對於同僚的勸說、這個士兵毫無理由的埋怨著。雖然腳沒有動作、但是視線已經瞥視起了周圍。這是周圍散發著陰氣的兵隊、用著一樣陰沉的表情默默走著。這裡沒有反對的意思。倒不如說、都豎起耳朵聽著這個點燃導火線的男人的話、甚至有著催促之意。

「明明平靜了在將要撒麥子的時候、卻進行徵兵。贊成這種計劃的傢伙、這裡面怎麼可能有」

說著語氣變強了、周圍發出了賛同之聲。

「啊啊、本來就是」
「上面的人根本就不關心我們、只是當成能夠榨取麥子的皮囊罷了」
「就算是耕田、撒種、親手培育、到頭來收穫完後也沒剩下多少⋯⋯」

相似的話在周圍提起、注意到後士兵也投降著聳了聳肩。這裡的士兵、都不是專業的兵。他們擅長使用的不是槍而是鋤頭、是不想看到敵兵的面對大地的農民。只是因為領主的命令才加入了軍隊、戰爭不是本意。

「就算是多少次也會說。貴族們就是蠢貨。明明奪走了我們的麥子、然後秋天到了、又把稅不夠的原因歸結到我們頭上」
「嗯、嗯」
「然後、就用不足啊什麼的理由、作為奴隷賣掉。我想、那貴族不過就是想進行買賣而已、唉」
「我的村子、最近經常能夠看到奴隷商人。現在應該是在尋找貨物吧」
「我也⋯⋯這場戰爭因為沒有利益、家裡最小的妹妹也可能要被賣掉了、村長說──」

一個年輕的士兵這樣抱怨、令周圍的大人同情的皺起眉頭。

「一去年的魔物騒亂以來、有好幾個村子都發生了這種事情⋯⋯」
「庫、你家的孩子被賣掉了拆東牆補西牆、已經算好了。近來、可是有好幾個村之的人都逃跑了吧？」
「確實。瓦爾登周邊的村一夜之間都空了、這些事情、也有聽過一些」
「村人全員、一夜潛逃。那麼、逃跑了之後、到哪裡了？」
「誰知道？估計往萊妮河去了」

萊妮河。聖加侖與隣國阿爾凱爾現在作為的國境線的河的名字。河的另一邊是厄魯皮斯・羅亞努──曾經的名字是厄魯皮斯・洛林。去年因為沃爾丹戰役、作為講和條約而割讓給了阿爾凱爾王國。

原來如此、現在前往敵國的領地、原聖加侖的土地。生活著許多聖加侖的人。農民越過國境的話、或許有些能夠交談的餘地。

但是、

「別說蠢話。那裡也就是阿爾凱爾人的土地了。那些傢伙、孤零零的逃過去結果肯定不比我們好」

一個士兵、從心底裏把對方當成了傻瓜。

厄魯皮斯・羅亞努現在是在阿爾凱爾王國的統治下。然後聖加侖、是對那個國家發動侵略戰爭、失敗的國家。這樣子、那些聖加侖人想要尋找新天地的僥倖跑去那裡、後果是非常難以想像的。沒有後盾的原敵國民、成為流民的末路、被受到礦山作為礦夫奴隷的可能性是最大的。同時、厄魯皮斯・羅亞努是有著礦脈的豊富土地。長年、作為兩大國爭奪的原因、便是如此。

如果逃去那裡流民就要成為奴隷。故此就算討厭也只能留下來。在連邦內部紛爭的驅使來、作為軍隊。
所以、這是他們無法逃跑的一個理由。

「──你們在說什麼？」

從頭頂上傳來的是盛氣凌人的聲音、軍隊抖了一下。聲音的源頭盛氣凌人是當然的。與徒步的軍隊兵隊相對、那個男人跨下有著軍馬。那是騎士。身上的鎧甲也是非常的漂亮、就算是與數個持槍的士兵戰鬥、也不一定會輸。那個人、穿著的鎧甲也能夠知道不是一般品。

「隊、隊長殿下⋯⋯」

用顫抖的聲音回答的、是最初发聊騷的士兵。之前罵著貴族的威勢、已經完全沒有了。
馬上的隊長用封閉形頭盔蓋住面部、雖然看不到臉但是能夠感覺到嘲笑。

「我說這速度有點慢。你們要是慢了、後列的士兵同樣也會變慢。這是利敵身為行為。⋯⋯該不會是打算在交戰中、給加雷寧方有利吧？」
「不、沒、沒有！絶對沒有！」
「大、大體上說、給加雷寧的人有利、我們也沒什麼好處啊⋯⋯」

其他士兵也一齊搖著頭。

加雷寧──屬於連邦的盟主自居、給加盟國各種麻煩的難題的古蘭敦布魯克大王國，去年因為敗戰導致威信下降、對中小國的影響力已經不如從前。在這緊箍鬆弛後、被施壓的一方也忍受不了。想要吞並沒有主見的盟主的、在連邦構成國也不是少數派。多國手裡也拿起了武器、盟主也用實力反壓──就這樣衝突開始了。

多數缺乏戰意的士兵、都是處於反盟主側的連邦構成國。
馬上的指揮官、氣勢洶洶的吹著鼻子。

「哼。那麼、快點前進。敵人不會等我們、我現在還是會有嘴溫柔的勸說。但是要是在犯錯、就用命來抵。給我記住了」
「『是、是的！』」

在命令下通軍隊又開始了從容的行軍。

聖加侖正是因為這種弱肉強食的風氣才成為了強國。也正是因為如此連邦就算是分裂了、他國威脅也比不上魔物。當然、在這軍隊裡的騎士、也是強者。

實際上、屬於這種程度的隊長的人材也是非常多的──雖然只是來農村抓壯丁、但是有著能夠與敵人戰鬥的力量。當然要是反抗或者逃跑的話、估計會被砍成碎片吧。有了這種想法、反抗的力氣也就沒有了。但是、沉默就意味著無形的恐懼。這也是那些士兵、不敢前往戰場的原因。

「沒有必要那麼陰沉。加雷寧的那些人、是受到威脅就不會戰鬥的弱兵。這次合戰、是為了給聯邦之間的交涉留下基礎。因此就算死去了、也沒什麼」

隊長說得更加的變本加厲，士兵的反應更加不好。居然是為了威脅就把他們抓過來、離開了自己賴以生存的田地。說如果拿起武器示威就能夠成功什麼的、可沒有感謝和安心的想法。
如同葬列的行軍進行著、在翻過了幾座丘陵後。

「那、那是⋯⋯！」

丘凌下的平野。東邊的地平線、視線前方有著陸續前進的軍隊。向著戰場的行軍隊伍直直走來、正體不明的軍團。毫無疑問那是屬於加雷寧的兵力。

「根據斥候的報告、那是半天剛到的⋯⋯」

撫摸著下巴的隊長。恐怕、是感悟到敵人要進攻了、無意間加快了行軍速度。這最初的平野戰場、是引誘自己前進的陷阱嗎。不管怎麼樣、要是遭遇了敵人、那就只有一個選擇。
快速的命令傳令騎兵過來。

「傳令。後方本陣發現敵人的身影。總勢⋯⋯大約二萬。我方有利」

敵人的數量是二萬、從斥候的情報來看從眼下的土煙量是正確的。自軍的數量是三萬五千人。和反盟主側諸侯的連合軍。在沃爾丹戰役消耗了太多的士兵、一時間募集不到這麼多人。不過古蘭敦布魯克大王國的專橫的反抗者看來也很多啊。

反過來說古蘭敦布魯克側遠征軍如此寡兵、也能夠知道其支持的勢單力薄。二萬的兵力確實不是威脅、但是去年的戰爭折損的主力中、溫存的太少了。連邦內又缺乏靠得住的伙伴、不給多一點士兵。

（加雷寧人的天下、不會長久的）

頭盔下、隊長的顏侮蔑著扭曲了。明明活用數量優勢就能夠在這個交戰獲得勝利、古蘭敦布魯克的專橫可見一斑。
無論是士氣低落的士兵、還是反盟主側軍勢的指揮官、都對自己勝利的未來毫不懷疑。

這時。

「那、那個是⋯⋯！？」

人個士兵、看到盟主側的軍隊的身影、聲音不由得抽筋。那個視線看到的、是敵軍中比較輕裝歩兵的隊列。不對、正確的來說那個隊伍裝備的武器。
槍很短、也沒有刀尖、硬要說的是就像是玩具一樣的武器──火槍。對此確認後的隊長、不愉快的吹著鼻子。

「原來如此、沃爾丹戰役中阿爾凱爾方用的新兵器嗎」
「喂、喂。那個是、我親眼看到了那個的殘酷。我從兄弟的約翰被那個打中了、那個傷口就──」

想起去年在戰場體驗到的那個威力、士兵中的幾個人就不由得臉青發抖。那次戰爭送去沃爾丹的士兵、只要是聖加侖加雷寧外的領邦國家為中心。因此、此度的反盟主陣營中、有著少數的那場戰爭生存者混在裡面也不奇怪。

聽聞當時使用那些兵器的、都是被徵兵的農兵──也就是說與聖加侖人相比不習慣戰爭的阿爾凱爾人──使用啦那個、都能夠令遠征軍吃苦頭。那麼要是古蘭敦布魯克大王國、裝備了那個東西的話。

但是、隊長不理那副狼狽的樣子。

「士兵啊、不要害怕」

傲慢的昂首挺胸、有強烈的語氣說著。

「我也聽說過那個奇妙的武器的報告。但是、害怕是沒有必要的。因為、這次聖加侖是在平野。不是在山下的盆地」

沃爾丹戰役的最終局面、就是在克萊維爾盆地戰鬥。那是火槍在伊托瑟拉大陸的歴史上首次出現、猛威振奮的戰場。但是、那個地方在面對敵人前事前就對狹隘的盆地事前進行了構築、等待著敵人的到來。就算靠近輕裝貧弱的飛行道具、也會有守護的牆壁⋯⋯⋯

反過來看這次。這次的戰場是騎兵與歩兵能夠輕易通過的平原。而根據斥候的報告、也沒有發現去年戰鬥用的防塁。也就是說咦接近的話這些無防備的銃兵就死定了。

「總之、那個火槍、只要一發就要再次裝填的吧？然後還要對筒進行掃除。也就是說作為防御的東西、在這個戰場上沒有」
「確、確實⋯⋯」
「加之、那次敗戰的主要原因也是魔物的亂入。像是吸血鬼那種規格外的東西。⋯⋯白晝堂堂、這個視野很好的戰場、魔物怎麼可能會出現？」

因此最大的原因是這個。實際上、阿爾凱爾軍的銃兵面在那種距離下根本無法防御、那是為了防止逃跑而設計的。只要那樣下去的話也是能夠打贏聖加侖的、這邊也是同樣的情況。就算是後背沒有魔物的大群攻擊、也是同樣的結果。
加雷寧的人們、戰訓分析錯誤──這是隊長的感想。

「敵人因為物珍奇的新品玩具而過於驕傲了。⋯⋯這次交戰、定會勝利」

穿著厚重的鎧甲，身經百戰的輕浮的隊長在此斷言。那句話、令周圍的士兵表現出安心的表情。不用在意、反正是無可避免的、只要經過這一戰就可以贏了。就算是輸了犧牲也會比較少。

※※※

戰場上、喧鬧與破裂聲が不絶於耳。隨著每次跟著聲音的火花四散、都會有誰的生命與血肉共同四散。

「跟、跟說的根本不一樣⋯⋯」

一個士兵、臉上已經被冷汗與淚水弄濕了、氣喘吁吁。

「為、為什麼火槍、可以連射！？」

說完他看著、盟主古蘭敦布魯克大王國方的軍勢。射出的火槍子彈、隨著炸裂後與硝煙一同飛出。這個射擊、事前隊長說過、有著入彈的攻擊間隔、然而現在卻看不到。無論是歩兵的行進還是騎兵稍微突擊、都豪不區別的倒在了那鉛彈的驟雨下。

當然、連邦盟主並沒有開發出能夠連射的魔法銃──事實如此。
只是把陣型、進行簡單的調換而已。

「銃士隊第一列、齊射完了」
「嗯。接下來第二列、前進！」
「『是！』」

隨著指揮官的信號、複數層的橫隊開始前後交替。彈盡的層就到後面去。而在後方的隊列、交替到前面射擊。反盟主側的軍隊面對射擊、還沒有接近就被交換的橫隊再次射擊、接下來子彈裝填又完畢了。

相互交換射擊。這就是、造成無限連射錯覺的正體。
其效果如同所見一樣。反盟主軍無法靠近、往古蘭敦布魯克軍的銃士隊前的速度就跟尺蠖一般慢。腳下滾動著的、是無數人與馬的屍體。然後被軍靴踩踏著。這是字面意義上的蹂躪。
看到這樣的景象，軍隊已經沒有人敢戰鬥了。

「不、不要⋯⋯要死了！！」

某個士兵呼喊著。那是在奔潰邊緣的絶叫。

「這、這種戰鬥、要怎麼打！我、我已經走不動了！」
「回、回去！我、我要回去啊！！」
「等、等一下、你這混蛋！？不許逃離！喂、給我聽──」

指揮官的怒鳴阻止不了混亂、在那連續不斷的銃聲下消失了。反盟主軍的士兵、聽到那個轟音身體就顫抖。

銃聲。那爆發聲音。每次聽起來、就像是哪個人死傷了一樣。這個在戰場上的正體、名為恐懼。那是對鉄砲的、鉄砲聲的恐懼。
恐懼。那是與有實際殺傷力的子彈並排、令火槍取得壓倒性的要素之一。槍口瞄準的瞬間、槍聲鳴響的瞬間、都令毎個士兵鋭氣挫失。即使捂住耳朵都那個聽到的爆音、既是死神的吼叫聲。為了能夠忍耐住那種東西、聖加侖人的士兵在發射時也要讓一般人類遠離。更何況、他們大部分都是今天才體驗到銃的恐怖。

「切、這些雜兵都害怕起來了⋯⋯！」

看到已經大勢已去的自軍、隊長咬牙切齒。事已至此、要重整崩壊軍隊非常困難。要是膽小一些的督戰官隨著投降都有可能。士兵們已經被槍的恐怖支配著。只能催促著、在敵人進攻之前阻止撤退了。

這也是、能夠在挽回敗局重整旗鼓的方法之一──、

「喂、遠距離人給我聽著音！近距離人給我看著！騎士的靈魂、就在這裡！」

說著面對無數的爆音踢了踢馬腹、隊長進行突擊。

──把敵人殺了。如果能夠達到這種結果、發現對方被斬殺的話、士兵也會重整旗鼓吧。

快速的、以眼睛可見的速度往盟主軍的隊列接近。當然、敵方立刻用銃火迎擊、飛來的子彈射在裝甲表面濺射出火花。

「沒有效果哦、終究就是鉛製的小石子罷了！」

頭盔的深處浮現的是獰猛的嘲笑。能夠與凶惡魔物戰鬥的騎士甲冑。這個防御力可不是輕裝的士兵能比的。當然、騎著的馬的馬鎧、也有著相應的厚度。用火槍齊射擊確實對于蹂躙雜兵來說是最合適的、但是對有著強力的個體來說卻例外的無力。確信這個事實後便往著敵陣衝鋒、

「≪火球≫」
「≪火球≫」
「≪火球≫」

⋯⋯途中、飛來的魔法攻擊擊中了讓自己從馬上掉落了下來。

「庫、哈啊⋯⋯！？」

肩部與胸口感受到了激痛、地面在旋轉著。雪解後不久沾滿濕氣的大地、在碰到熱的裝甲後冒著蒸汽。不只是身體、就連之前胯下的愛馬、也連悲鳴都沒有發出就倒在了地上。

「咕嗚、嗚⋯⋯」
「呵。就像是割草一樣對雜兵進行齊射、如果有能夠忍耐銃彈的就用魔法攻擊。確實非常有效率的戰術啊」

苦痛殘喘的隊長面前、有誰說了這句話。立領的外套、完全沒有受到戰塵污染的白領結。還有、左胸刺繍著黃金的雙頭鷲。與戰場不相稱的豪爽身影。腰上掛著的劍、能夠勉強看得出男人是個練家子。但是、他與士兵和騎士不一樣、那是一眼就能夠看得懂的裝扮。

「這、這個紋章──大王親衛騎將！？」

看清男人正體的隊長、驚愕的呻吟著。

大王親衛騎將──尚武之國・聖加侖連邦王國的最高戰力。古蘭敦布魯克大王國、海德爾雷福特王朝只有歴代大王才能對付的、大陸東方最強的騎士。總共不到六人、每一人都是一騎當千的強者、權限與將軍同等的例外存在。

平靜的把這種怪物投入戰爭、說明了這次面對連邦內亂，盟主明確的表示了，認真的態度。
在不寒而慄的隊長前面、男人優雅的行了一禮。

「啊、忘記自我介紹抱歉了。我是奧斯卡・萊魯特・馮・海格道夫。如你所見、是古蘭敦布魯克大王國軍的大王親衛騎將、擔任末席。以後、多多關照」

（別、別開玩笑了這個男人⋯⋯！）

咯嘰、臼齒破裂了。明明對方都趴在地上了、還說什麼禮儀。假惺惺也要有個限度吧。不由得一股郁悶。

「相當的、游刃有餘、是吧？」
「呼嗯？」

面對這苦痛的惡態、叫做海格道夫的人眼光閃。

「游刃有餘、什麼也是？我和你說些無關緊要的話、有什麼不適合的嗎？」

這個男人在扯淡、隊長這樣想著。連鎧甲都沒穿、打扮成這個有著、在戰場的中心停下腳步。如果說是類似於弓兵的魔導師還好。魔導戰力畢竟不是古蘭敦布魯克側專屬。魔導學院的人也是屬於高傲之輩、因此那也是相應的舉止。

（就被魔法燒掉、後悔好了！）

僅僅是初戰就受到了大打擊、這次合戰的勝負已經分明了。但是、至少能夠取走眼前這個屬於大王的走狗的命。只要我方的魔法打過來、拼死就能夠殺死對方⋯⋯⋯

他看到海格道夫笑了起來。那是刻薄的、冷酷的笑顏。

「有一件事情忘記說了⋯⋯你那邊的魔導師、已經全部不在了哦」
「⋯⋯哈？」
「並不是不思議的事情。畢竟銃士隊是比起魔法還要損耗更多的東西、要是那樣我軍不就會大損了？所以、在你們受到銃擊混亂之際、已經開始狩獵魔導師了。那是我做的」

虛張聲勢。不管怎麼樣。就算是親衛騎將的戰力、要在這麼短的時間內解決。也是不可能的。

但是、不管等了多久聽到的都是槍聲、槍聲、槍聲。然後是古蘭敦布魯克方的歡聲、自軍的悲鳴。偶然產生的魔法炸裂音、大概也是在古蘭敦布魯克側隊列的前面──與隊長的方向相同、只有銃士隊進行突擊還有反盟主軍的騎士被狙擊的情況。

臉上的血色消失了。對此、海格道夫那愉悅的笑容更深了。

「理解了嗎？那麼、選擇吧。降伏、或者是抵抗」

事已至此、處於劣勢側選擇什麼。海格道夫都覺得、這兩者都沒關係。眼前的對手要是選擇、那就不用殺掉省事。要是選擇抵抗的話──、

「別開玩笑了！作為騎士、怎麼可能連用劍交手都不到一回合就投降！」
「呼呢？是嗎」

隊長起身猛撲過去。買沒有接近對方的軀幹就被一閃砍死。

「但是、以騎士來說的話、這麼說吧⋯⋯不知的雙方戰力差距的匹夫、是不適合作為騎士的」

那是以眼睛跟不上的速度揮出的劍、然後抖了抖血放入鞘內。咚嘶、那是屍體落地的聲音。那個俯視的視線、比白刃還要銳利、還要冰冷。

──輸掉的騎士要是選擇抵抗、對大王親衛騎將來說、也沒有多少麻煩。總之、事實如此。

「但是、嘛、怎麼說。實力差距是如此的壓倒性、真是無聊啊」

海格道夫對著周圍的戰況看了一眼。

現在的趨勢、完全傾斜到了盟主軍的方向。當初與數量佔優勢的反盟主軍、在火槍兵的射擊下處於奔潰狀態。即使有數量、也只是烏合之眾。就像是踢散鴉群一樣。如果能夠稍微的整頓一下指揮系統、提高一下士兵的熟練度、組織別動隊奇襲銃士隊進行混戰也不是不可能。但是、那是無法模仿的、去年的戰爭的連邦非主流派在沃爾丹投入後、便消耗過多。

想到比想像以上的輕鬆後不由得掃興、明明友軍認真的進擊還沒有開始。其中、

「真是漂亮、將軍」

在本陣的方向、一位騎馬的將領、過來打招呼。

「海格道夫殿下」
「哎呀哎呀、真是驚訝。這場戰鬥、雖然缺席會勝利、但是容易簡單的擊破敵軍、卻沒有預料到」
「⋯⋯如此褒獎、不勝惶恐。是啊、有了閣下的助力、士兵們也勇猛起來了」

用了基本的禮貌說法回答了將軍。但是、表情卻有點僵硬、無精打采的、散發著笨拙與頑固。這是當然的。這個將軍與屬於大王親衛騎將的海格道夫不一樣、不是古蘭敦布魯克大王國土生土長的軍人。只是連邦構成的領邦群的這次、參加了盟主側這一邊而已。總而言之就是外人。故此、這個人對大王國軍、明顯有著距離感。

面對這個男人、海格道夫表面上還是親切的拉開嗓門。

「不不！不用謙虛、將軍！有著這個記録和戰果、完全都是你的獻策與指揮哦、將軍！挺起胸膛的自誇吧。這樣是對在你麾下戰鬥的士兵的禮貌」
「哈⋯⋯」

對此將軍的回答、是垂頭喪氣。
正是如此。這次連邦內亂裡站在了盟主側的一邊、說到底也是作為他故郷的連邦構成國、之間的利益所致哪有對古蘭敦布魯克大王國的忠心啊。

但是、這個不能說出來。古蘭敦布魯克、還有大王、這場戰鬥聖加侖連邦之間會如此曖昧、就是為了明確的進行再編為一個國家。然後連邦內部的權利鬥爭、與這個不一樣。戰後的窺視、有這種能力的軍人越多越好。

所以他大聲主張。這場戰都給盟主最大貢獻的便是你。讓敵兵流最多血的也是你。這樣他的立場、就無法與古蘭敦布魯克撇清關係了。
聞到了政治的臭味、馬上的將軍打了可咳嗽轉移視線。

「這麼說、你。對了、對殘敵進行追擊來說」
「嗯。追擊啊⋯⋯」

論起這場的貢獻、海格道夫也是同樣的。他本質上是一個武人。因為口舌之爭令眼前的敵人逃跑了、也不是他的本意。

「看起來、是初戰的衝擊過頭了、結構潰走的敵人也跑到非常遠啊」

銃擊戰對前陣的意識粉砕是非常成功的、後面的人、看到形勢不利的前線便混亂起來、直接遁走了。這種沒有秩序的逃跑方式、著實會拉長行軍的距離。除了落伍的士兵、要殲滅的逃跑的士兵、有著相當的數量。

「火槍兵是歩兵。不僅如此、槍聲會讓馬動揺、因此騎兵也沒有多帶」

馬是膽小敏感的動物。只要耳朵沒聾聽到槍聲在戰場響起、就會但怯起來而無法操縱。對魔物、對魔導師特別調練的馬、以及靈獸之類外、一般騎兵都不會配備。

「為了令馬習慣那個聲音、戴上耳罩的方法、好像要開始實用化了」
「原來如此。看來有對策了」
「不不。⋯⋯嘛、比起與槍兵連攜、還有向上的餘地。例如──」

說完、指了指地面畫了起來。

──例えば、敢えて銃兵の發砲を遅らせて敵を引き付ける。そうすれば後退に時を要するようになり、より殲滅が容易となるだろう。無論、ただ擊たずにいれば肉薄されてこちらに被害が生じるのは明白。そこで效いてくるのが槍兵だ。敵戰列に文字通りの橫槍を入れることで、銃兵の戰列を守ると同時に拘束する。これもまた銃の火力で殲滅を圖る工夫。

ここへ更に騎馬隊の突擊を加えれば、敵は陣形を千々に亂し、加えて槍兵に身動きを妨げられている中、銃彈の雨に身を晒すことになる。

「──不過現在來說、還是空談罷了」

聽到這個結論的將軍、眼光炯炯有神、無法反駁。
一定會的。這個新戰術、一定能夠使用。那個幹勁、表情就像是在這樣說。
自古以來、優秀的兵法家、那個手段會被比喻為魔法。他說的戰術是否能夠真發揮效果、還是個未知數⋯⋯但是對方卻願意去為他赴死、流血、某種程度上來說、這也是一種詛咒魔法。

「那麼、等國內統一的時候、在準備一下吧？」
「⋯⋯問題、沒有」

這句話、令海格道夫喜色的臉扭曲了。

「可以了！已經非常厲害了將軍っ！托你獻身的福、我國的雄飛可能就更上一層樓了！要是成功了、以後也請多多關照了、好嗎？」

是對戰爭異樣的情熱嗎？還是受到了對方的詛咒？這些問題沒有任何意義。只要戰意的矛頭指向自軍的敵人、那就可以了。就連古蘭敦布魯克是否能夠統一聖加侖也無所謂。也就是說、這個將軍也是屬於奇貨可居啊。海格道夫這樣判斷。

「⋯⋯嗯」

將軍、眼睛裡寄宿著黑炎。在知道了更有效率的殺死敵兵的方法手、沉醉在了深闇的顏色中。只要有人看到、都不由得會覺得、這是情念滿滿的光輝。

「這次內亂結束後、就要開始對阿爾凱爾進行一雪前恥了」
「嗯。真是非常可靠呢、鮑爾將軍」

最後、他加入了追擊戰、在海格道夫面前消失了。
尤爾根・鮑爾。拜哈里耶王國出身的將軍。沒錯、去年沃爾丹戰役的敗軍之將、便是他。一敗塗地的外樣將軍、不知為何官復原職了。
然後、

（⋯⋯去年以來、每次做夢都想遠離火槍（那傢伙）的聲音。知道現在。能夠攻略的方法、終於找到了）

他在聖加侖全軍中、對這種兵器是最有經驗的、面對這個可是煞費苦心。

鮑爾騎著馬前進著、看著眼下的戰場。現在他們的樣子、是粉砕敵軍的銃士隊之姿。以及如同無力的野鳥般亂竄的敵軍之姿。那個反盟主軍的士兵之中、說不定就有去年秋天、與鮑爾共同在克萊維爾的共赴戰場的人。用自己的手踐踏他們的罪惡感、說沒有是騙人的。

但是、他現在不能猶豫不前。

（新的兵器、新的技術、新的時代。時代變了。我們不能就這樣被落下）

與沃爾丹戰役的敗北不同、聖加侖的領邦群明顯是衰退了。這是無論如何悔恨都無法避免事實。今後、連邦的政局、在戰力的溫存著迎來終戰的盟主古蘭敦布魯克、其勢力會是最強吧。

失去了許多士兵的拜哈里耶王國今後要走的道路、必定是一條艱苦的路。只能不停的忍耐、在古蘭敦布魯克面前低頭、寄宿在其傘下。這個遵循這個選擇與功績、連邦今後形成的新秩序中處於有利地位。

當然、對於一年前利用自己的對方、拜哈里耶國內的反對聲也非常大。不、是反盟主的發言屬於多數派。但是、要是讓憤懣蔓延的話便會無路可走。

不幸中的萬幸是、戰役敗北後受到虜囚之辱的鮑爾、講和之後在國內並沒有從此被架空。並沒有因為位居閑職而從此墮落下去、作為消遣──靠著那個餘溫──寫出了、根據實戰的體感得出的火槍的威脅與弱點、那個有關運用法的論文。古蘭敦布魯克軍都不知道那是一個怎麼樣的經過。因此這次遠征之際、作為試驗的銃士隊的指揮、那個先鋒。

遠征軍四萬人過半數喪失的大敗、這是一個能夠令人感嘆的不幸。但是、不知道在什麼機緣下、現在成為了拜哈里耶親盟主派閥的軍代表。能夠熟練運用聖加侖軍的新兵器的將領只要他一人。當然、去年遭受敗北的他現在立場非常弱小。無論什麼時候會能夠別母國與盟主捨棄⋯⋯處於這樣的立場、因此必須要有功績。比如像這個鎮壓連邦內亂一樣。

（首先是在連邦內部鞏固地位、敗戰動搖的我在國家的立場上必須補強。解決後顧之憂後──）

鮑爾は、用著鋭利的眼光看著西方的天空。那前面、是這次舉兵的領邦的諸侯原本的領土。不、是因縁的土地厄魯皮斯・羅亞努。然後、更加前面的是沃爾丹。

（──等著我哦、阿爾凱爾王國。還有那個叫歐布尼爾的混賬貴族。沃爾丹不取我頭顱的後果、今後必定會令你們後悔⋯⋯！）

蒼天之下、那是明媚的春光之中、但是那個眼中寄宿的光比黑夜更黑暗逼冬天更冷。尤爾根・鮑爾、等待著復仇的時刻、度日如年的等待著。